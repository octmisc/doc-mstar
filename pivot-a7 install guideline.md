## pivot-a7 install guideline 

### 1 download tools chains

链接：https://pan.baidu.com/s/1lS4diNF1dopyCqF6RxsZbQ 
提取码：x1z8

### 2 install compiler

####   2.1 把arm-buildroot-linux-uclibcgnueabihf-4.9.4.tar.xz 解压到/opt

```
   cd mstar
   $ xz -d ***.tar.xz
   $ tar -xvf ***.tar
   $ sudo mv arm-buildroot-linux-uclibcgnueabihf-4.9.4 /opt
```

####  2.2 vi /etc/profile，

```
 在文件最后添加：

 export PATH=$PATH:/opt/arm-buildroot-linux-uclibcgnueabihf-4.9.4/bin，

然后source /etc/profile使路径立即生效。
$ "$CROSS_COMPILE"gcc -v
```

### 3 install libiconv

####    3.1 $:tar -zxvf libiconv-1.16.tar.gz

####    3.2 配置和编译：  

```
$: ./configure --prefix=/usr/local,
$: make,
$: sudo make install   (权限不够可以加sudo)
```

####    3.3 做软连接：

```
sudo ln -s /usr/local/lib/libiconv.so.2 /usr/lib/libiconv.so.2，
         sudo ldconfig (权限不够可以加sudo)
```

