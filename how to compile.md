## how to compile 

### 1 clone code

```
mkdir a7

cd a7

git clone  git@gitee.com:mstar4/doc.git

git clone  git@gitee.com:mstar4/boot.git

git clone  git@gitee.com:mstar4/kernel.git

git clone  git@gitee.com:mstar4/project.git

git clone  git@gitee.com:mstar4/sdk.git
```

### 2 chmod 

```
cd boot

find -name  "*sh" | args chmod 777

chmod 777 mkimage
```



```
cd kernel

find -name  "*sh" | args chmod 777

cd scripts

chmod 777 mkimage

chmod 777 mz
```



```
cd project

find -name "*sh" | xargs chmod 777

find -name gen_init_cpio | xargs chmod 777

find -name mkfs.jffs2 | xargs chmod 777

 find -name "fixdep" | xargs chmod 777
  find -name "modpost" | xargs chmod 777
```

###  3 make

 

```
  cd boot

   ./build.sh

  cd kernel

  ./build.sh

  cd project

  ./build.sh

  cd sdk/driver

   make
```

